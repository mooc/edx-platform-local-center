define(["js/views/paging", "js/views/video", "js/views/paging_header", "js/views/paging_footer"],
    function(PagingView, VideoView, PagingHeader, PagingFooter) {

var VideosView = PagingView.extend({
    // takes VideoCollection as model

    events : {
        "click .column-sort-link": "onToggleColumn"
    },

    initialize : function() {
        PagingView.prototype.initialize.call(this);
        var collection = this.collection;
        this.template = _.template($("#video-library-tpl").text());
        this.listenTo(collection, 'destroy', this.handleDestroy);
        this.registerSortableColumn('js-video-name-col', gettext('Name'), 'display_name', 'asc');
        this.registerSortableColumn('js-video-date-col', gettext('Date Added'), 'date_added', 'desc');
        this.setInitialSortColumn('js-video-date-col');
    },

    render: function() {
        this.$el.html(this.template());
        this.tableBody = this.$('#video-table-body');
        this.pagingHeader = new PagingHeader({view: this, el: $('#video-paging-header')});
        this.pagingFooter = new PagingFooter({view: this, el: $('#video-paging-footer')});
        this.pagingHeader.render();
        this.pagingFooter.render();

        // Hide the contents until the collection has loaded the first time
        this.$('.video-library').hide();
        this.$('.no-video-content').hide();

        return this;
    },

    renderPageItems: function() {
        var self = this,
            videos = this.collection,
            hasVideos = videos.length > 0;
        self.tableBody.empty();
        if (hasVideos) {
            videos.each(
                function(video) {
                    var view = new VideoView({model: video});
                    self.tableBody.append(view.render().el);
                });
        }
        self.$('.video-library').toggle(hasVideos);
        self.$('.no-video-content').toggle(!hasVideos);
        return this;
    },

    handleDestroy: function(model, collection, options) {
        this.collection.fetch({reset: true}); // reload the collection to get a fresh page full of items
        analytics.track('Deleted Video', {
            'course': course_location_analytics,
            'id': model.get('url')
        });
    },

    addVideo: function (model) {
        // Switch the sort column back to the default (most recent date added) and show the first page
        // so that the new video is shown at the top of the page.
        this.setInitialSortColumn('js-video-date-col');
        this.setPage(0);

        analytics.track('Uploaded a File', {
            'course': course_location_analytics,
            'video_url': model.get('url')
        });
    },

    onToggleColumn: function(event) {
        var columnName = event.target.id;
        this.toggleSortOrder(columnName);
    }
});

return VideosView;
}); // end define();
