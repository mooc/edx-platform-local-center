import logging
from functools import partial
import math
import json
import os
import time
from datetime import datetime as etime

from django.conf import settings
from django.http import HttpResponseBadRequest
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django_future.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_POST

from edxmako.shortcuts import render_to_response
from cache_toolbox.core import del_cached_content

from mooc.models import Course_Video

from xmodule.contentstore.django import contentstore
from xmodule.modulestore.django import modulestore
from xmodule.modulestore import Location
from xmodule.contentstore.content import StaticContent
from xmodule.util.date_utils import get_default_time_display
from xmodule.modulestore import InvalidLocationError
from xmodule.exceptions import NotFoundError
from django.core.exceptions import PermissionDenied
from xmodule.modulestore.django import loc_mapper
from xmodule.modulestore.locator import BlockUsageLocator

from util.json_request import JsonResponse
from django.http import HttpResponseNotFound
from django.utils.translation import ugettext as _
from pymongo import ASCENDING, DESCENDING
from .access import has_course_access

log = logging.getLogger("edx.student")
AUDIT_LOG = logging.getLogger("audit")

__all__ = ['videos_handler']


@login_required
@ensure_csrf_cookie
def videos_handler(request, tag=None, package_id=None, branch=None, version_guid=None, block=None, video_id=None):
    """
    The restful handler for videos.
    It allows retrieval of all the videos (as an HTML page), as well as uploading new videos,
    deleting videos, and changing the "locked" state of an video.

    GET
        html: return an html page which will show all course videos. Note that only the video container
            is returned and that the actual videos are filled in with a client-side request.
        json: returns a page of videos. The following parameters are supported:
            page: the desired page of results (defaults to 0)
            page_size: the number of items per page (defaults to 50)
            sort: the video field to sort by (defaults to "date_added")
            direction: the sort direction (defaults to "descending")
    POST
        json: create (or update?) an video. The only updating that can be done is changing the lock state.
    PUT
        json: update the locked state of an video
    DELETE
        json: delete an video
    """
    location = BlockUsageLocator(package_id=package_id, branch=branch, version_guid=version_guid, block_id=block)


    if not has_course_access(request.user, location):
        raise PermissionDenied()

    response_format = request.REQUEST.get('format', 'html')
    if response_format == 'json' or 'application/json' in request.META.get('HTTP_ACCEPT', 'application/json'):
        if request.method == 'GET':
            return _videos_json(request, location)
        else:
            return _update_video(request, location, video_id)
    elif request.method == 'GET':  # assume html
        return _video_index(request, location)
    else:
        return HttpResponseNotFound()


def _video_index(request, location):
    """
    Display an editable video library.

    Supports start (0-based index into the list of videos) and max query parameters.
    """
    old_location = loc_mapper().translate_locator_to_location(location)
    course_module = modulestore().get_item(old_location)


    return render_to_response('video_index.html', {
        'context_course': course_module,
        'video_site': request.META['HTTP_HOST'],
        'video_callback_url': location.url_reverse('videos/', '')
    })


def _videos_json(request, location):
    """
    Display an editable video library.

    Supports start (0-based index into the list of videos) and max query parameters.
    """
    course_str = str(location).split('/')[0].replace('.', '/')
    items = []

    requested_page = int(request.REQUEST.get('page', 0))
    requested_page_size = int(request.REQUEST.get('page_size', 5)) #50
    requested_sort = request.REQUEST.get('sort', 'date_added')
    sort_direction = DESCENDING
    if request.REQUEST.get('direction', '').lower() == 'asc':
        sort_direction = ASCENDING

    orderbydata = ''
    if sort_direction == DESCENDING:
        orderbydata = '-'

    if requested_sort == 'date_added': 
        orderbydata = orderbydata + 'date_added'
    elif requested_sort == 'display_name': 
        orderbydata = orderbydata + 'display_name'
       
    current_page = max(requested_page, 0)
    start = current_page * requested_page_size
    coursevids = Course_Video.objects.filter(course_id=course_str).order_by(str(orderbydata))  
    items = coursevids[start:start+requested_page_size]
    videos, total_count = list(items), len(coursevids)
 
    end = start + len(videos)


    
    # If the query is beyond the final page, then re-query the final page so that at least one video is returned
    if requested_page > 0 and start >= total_count:
        current_page = int(math.floor((total_count - 1) / requested_page_size))
        start = current_page * requested_page_size
        items = coursevids[start:start+requested_page_size]      
        videos, total_count = list(items), len(coursevids) 
        end = start + len(videos)
     
 
    video_json = []
    for video in videos:
        vid = video.id
        v_url = video.url_coursevideo
        v_host = request.META['HTTP_HOST']
        v_protocol = settings.COURSE_VIDEO_PROTOCOL
        v_portable_url = u"{protocol}://{host}{url}".format(protocol=v_protocol,host=v_host,url=v_url)
        thumbnail_location = None
        video_locked = video.lock
        video_json.append(_get_video_jsonsql(video.display_name, video.datatime_UTC, v_url, v_portable_url, thumbnail_location, video_locked,vid))

     
    return JsonResponse({
        'start': start,
        'end': end,
        'page': current_page,
        'pageSize': requested_page_size,
        'totalCount': total_count,
        'videos': video_json,
        'sort': requested_sort,
    })


@require_POST
@ensure_csrf_cookie
@login_required
def _upload_video(request, location):
    '''
    This method allows for POST uploading of files into the course video
    library, which will be supported by GridFS in MongoDB.
    '''
    old_location = loc_mapper().translate_locator_to_location(location)

    file_url = None
    courseVid = None

    # Does the course actually exist?!? Get anything from it to prove its
    # existence
    try:
        modulestore().get_item(old_location)
    except:
        # no return it as a Bad Request response
        logging.error("Could not find course: %s", old_location)
        return HttpResponseBadRequest()

    try:
        course_dir =  str(location).split('/')[0].replace('.', '/')
        course_str = str(location).split('/')[0]

        upload_video_file = request.FILES['file']
        #video_filename = upload_video_file.name
        video_filename, ext = os.path.splitext(os.path.basename(upload_video_file.name))

        video_content_type = upload_video_file.content_type 
        
        # see if there is a valid 'content_type'  
        if not video_content_type in list(settings.COURSE_VIDEO_CONTENT_TYPE):
            return HttpResponseBadRequest()

        # compute a unique 'filename' to a url 
        video_filename_add = video_filename + str(int(time.mktime(etime.now().timetuple()))) + ext
        
        while Course_Video.isExist(display_name=video_filename_add): 
            video_filename_add = video_filename + str(int(time.mktime(etime.now().timetuple()))) + ext
        file_url = os.path.join(settings.COURSE_VIDEO_ROOT, course_dir, video_filename_add)
         

        # see if directory path exists as well, hence create it
        dir_url = os.path.dirname(file_url)
        if not os.path.exists(dir_url):
            os.makedirs(dir_url)

        # ok, save the content to file system 
        with open(file_url, 'wb+') as destination:
            for chunk in upload_video_file.chunks():
                destination.write(chunk)

        # see if there is a valid 'content'
        with open(file_url, 'rb') as data:
            datavideotype = data.read(11)
            if not datavideotype[6:] in list(settings.COURSE_VIDEO_CONTENT): 
                raise Exception
        
        courseVid = Course_Video() 
        courseVid.user = request.user
        courseVid.course_id = str(course_dir)
        courseVid.filelocation = os.path.join(course_dir, video_filename_add)
        courseVid.lock = 0
        courseVid.display_name = str(video_filename_add)

        # ok, save the details
        courseVid.save() 

        cv_url = courseVid.url_coursevideo
        cv_host = request.META['HTTP_HOST']
        cv_protocol = settings.COURSE_VIDEO_PROTOCOL
        cv_portable_url = u"{protocol}://{host}{url}".format(protocol=cv_protocol,host=cv_host,url=cv_url)
        
    except Exception as e:
        if os.path.exists(file_url):
            os.remove(file_url)

        if courseVid and courseVid.pk:
            courseVid.delete()

        return HttpResponseBadRequest()


    response_payload = {
        'video': _get_video_jsonsql(courseVid.display_name, courseVid.datatime_UTC, cv_url, cv_portable_url, None, False, courseVid.id),
        'msg': _('Upload completed')
    }

    return JsonResponse(response_payload)


@require_http_methods(("DELETE", "POST", "PUT"))
@login_required
@ensure_csrf_cookie
def _update_video(request, location, video_id):
    """
    restful CRUD operations for a course video.
    Currently only DELETE, POST, and PUT methods are implemented.

    video_id: the key of the video (used by Backbone as the id)
    """
    if request.method == 'DELETE':

        try:
            courseVid = Course_Video.objects.filter(id=video_id)[0]
            file_url = os.path.join(settings.COURSE_VIDEO_ROOT, courseVid.filelocation)
            if courseVid.pk and os.path.exists(file_url):
                courseVid.delete() 
                os.remove(file_url)
            else:
                return JsonResponse(status=404)
        except Exception as e:
            return JsonResponse(status=404)

        return JsonResponse()        
    elif request.method in ('PUT', 'POST'):
        if 'file' in request.FILES:
            return _upload_video(request, location)
        else:
            # Update existing video
            try:
                modified_video = json.loads(request.body)
            except ValueError:
                return HttpResponseBadRequest()
            

            try:
                video_id = modified_video['id']
                video_lock = modified_video['locked']
                courseVidUpdate = Course_Video.objects.filter(id=video_id)[0]

                if courseVidUpdate and courseVidUpdate.pk:
                    courseVidUpdate.lock = video_lock
                    courseVidUpdate.save() 
                else:
                    return HttpResponseBadRequest()
            except Exception as e:
                return HttpResponseBadRequest()

            return JsonResponse(modified_video, status=201)


def _get_video_jsonsql(display_name, date, url, portable_url, thumbnail_location, locked, video_id):
    """
    Helper method for formatting the video information to send to client.
    """
    return {
        'display_name': display_name,
        'date_added': date,
        'url': url,
        'portable_url': portable_url,
        'thumbnail': None,
        'locked': locked,
        # for delete/update.
        'id': video_id
    }
