from django import forms



import re
from mooc.models import Course, Institute_Course
from django.core.validators import validate_email


class LocalAdminEmailForm(forms.Form):
    ''' 
    This form view details for  Request for Local Admin  
    Email field used  
    '''
    error_messages_doc = {
        'email': ("Enter a valid Email"), 
    }
    
    emailid = forms.CharField(max_length=75, label='Enter Email')
    

    def __init__(self, *args, **kwargs):
        super(LocalAdminEmailForm, self).__init__(*args, **kwargs)
        self.fields['emailid'].error_messages = {'required': 'Enter a valid e-mail address'}
          

    def clean_emailid(self):
        emailid = None
        try:
            emailid = self.cleaned_data['emailid']
            validate_email(emailid)
        except Exception as e:
            raise forms.ValidationError("Enter a valid e-mail address") 
         
        # your other validations go here.
        return emailid



    

class StudentCourseInstituteForm(forms.Form):
    ''' 
    This form view details select course/institute  
    '''
    error_messages_doc = {
        'acourse': ("Select a Course"),
        'ainstitute_course': ("Select a Institute"),
    }
    
    course = forms.ModelChoiceField(queryset=Course.objects.all(), label='Course', empty_label="Select Course")
    institute = forms.ChoiceField(choices=(), label='Institute Name', widget=forms.Select(attrs={'class':'selector'}) )

    def __init__(self, *args, **kwargs):
        super(StudentCourseInstituteForm, self).__init__(*args, **kwargs)
        self.fields['course'].error_messages = {'required': 'Select a Course'} 
        self.fields['institute'].error_messages = {'required': 'Select a Institute'}
        choices = [(int(ic.institute_id), unicode(ic.institute_name)) for ic in Institute_Course.objects.all()]
        self.fields['institute'].choices = choices

    def clean_course(self):
        course = None
        try:
            course = self.cleaned_data['course']
            if course and course in [None, '', 'Select Course',]:
                raise Exception 
        except Exception as e:
            raise forms.ValidationError("Select a Course") 
         
        # your other validations go here.
        return course

    
    def clean_institute(self):
        institute = None
        try:
            institute = self.cleaned_data['institute']
            if institute and institute in [None, '', 'Select Institute',]:
                raise Exception
        except Exception as e:
            raise forms.ValidationError("Select a Institute") 

         
        # your other validations go here.
        return institute




